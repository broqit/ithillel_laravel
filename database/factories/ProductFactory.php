<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => 0,
            'title' => $this->faker->word,
            'description' => $this->faker->paragraph,
            'short_description' => $this->faker->words(rand(80, 150), true),

            'SKU' => $this->faker->words(rand(35), true),
            'price' => $this->faker->numberBetween(1500, 6000),
            'discount' => $this->faker->randomDigit(rand(5,10)),
            'in_stock' => $this->faker->boolean(true),
            'thumbnail' => $this->faker->imageUrl( 200,  200)
        ];
    }
}
