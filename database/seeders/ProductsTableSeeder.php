<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::firstOrCreate([
            'category_id' => 0,
            'title' => 'Xiaomi Mi4',
            'description' => 'Описание телефона xiaomi mi4',
            'short_description' => 'Описание телефона xiaomi mi4',
            'SKU' => 'FRESDW',
            'price' => 3500,
            'discount' => 5,
            'in_stock' => true,
            'thumbnail' => 'images/xiaomi_4.png'
        ]);

        Product::firstOrCreate([
            'category_id' => 0,
            'title' => 'Xiaomi Mi5',
            'description' => 'Описание телефона xiaomi mi5',
            'short_description' => 'Описание телефона xiaomi mi5',
            'SKU' => 'ADESDW',
            'price' => 5500,
            'discount' => 5,
            'in_stock' => true,
            'thumbnail' => 'images/xiaomi_5.png'
        ]);
    }
}
