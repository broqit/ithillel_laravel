<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_role = Role::admin()->first();

        User::firstOrCreate([
            'name' => 'Roman',
            'surname' => 'Borkunov',
            'birthdate' => '1994-08-12',
            'email' => 'test@test.ru',
            'phone' => '+380931231212',
            'password' => Hash::make('pass_test'),
            'role_id' => $admin_role->id
        ]);

        User::firstOrCreate([
            'name' => 'Test',
            'surname' => 'Test',
            'birthdate' => '0000-00-00',
            'email' => 'test2@test.ru',
            'phone' => '+380931231242',
            'password' => Hash::make('pass_test'),
            'role_id' => $admin_role->id
        ]);

        User::factory(10)->create();
    }
}
